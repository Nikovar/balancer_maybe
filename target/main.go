package main

import (
	"io"
	"log"
	"net/http"

	//"strconv"
	"sync"
	"time"
)

var requests int

func main() {
	var wt sync.WaitGroup
	requests = 0
	go Timer()
	//serverStart("127.0.0.1:6660", &wt)
	http.HandleFunc("/", Index)
	log.Println("server on port", "127.0.0.1:6660", "started")
	log.Println(http.ListenAndServe("127.0.0.1:6660", nil))
	// for i := 0; i < 5; i++ {
	// 	wt.Add(1)
	// 	serverStart("127.0.0.1:666"+strconv.Itoa(i), &wt)
	// }
	wt.Wait()
}

func serverStart(port string, wt *sync.WaitGroup) {
	http.HandleFunc("/", Index)
	log.Println("server on port", port, "started")
	log.Println(http.ListenAndServe(port, nil))
	wt.Done()
}

func Timer() {
	for {
		select {
		case <-time.After(10 * time.Second):
			log.Printf("Requests operating :%d\n", requests)
		}
	}
}

func Index(w http.ResponseWriter, r *http.Request) {
	requests++
	//log.Println("received")
	time.Sleep(100 * time.Millisecond)
	requests--
	io.WriteString(w, "")
}
